<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:39+01:00
 */

$GLOBALS['TL_LANG']['avisota_salutation']['0'] = 'Sehr geehrter ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['1'] = 'Sehr geehrte ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['2'] = 'Lieber Herr  ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['3'] = 'Liebe Frau ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['4'] = 'Lieber Herr ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['5'] = 'Liebe Frau ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['6'] = 'Sehr geehrte/r Frau/Herr ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['7'] = 'Sehr geehrte/r Frau/Herr ##forename ## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['8'] = 'Sehr geehrte/r Frau/Herr ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['9'] = 'Sehr geehrte Empfängerin, Sehr geehrter Empfänger,';
