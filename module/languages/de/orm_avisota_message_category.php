<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-05-31T04:01:08+02:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_category']['salutation']['0']   = 'Anrede';
$GLOBALS['TL_LANG']['orm_avisota_message_category']['salutation']['1']   = 'Bitte wählen Sie die Anrede für Empfänger aus dieser Quelle aus.';
$GLOBALS['TL_LANG']['orm_avisota_message_category']['salutation_legend'] = 'Automatische Anrede';
