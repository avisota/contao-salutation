<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:22+01:00
 */


$GLOBALS['TL_LANG']['orm_avisota_salutation']['copy']['0']                       = 'Anrede kopieren';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['copy']['1']                       = 'Anrede ID %s kopieren.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['delete']['0']                     = 'Anrede löschen';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['delete']['1']                     = 'Anrede ID %s löschen.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['edit']['0']                       = 'Anrede bearbeiten';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['edit']['1']                       = 'Anrede ID %s bearbeiten.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['editheader']['0']                 = 'Anredeeinstellungen bearbeiten';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['editheader']['1']                 = 'Bearbeiten Sie die Einstellungen für die Anrede ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableGenderFilter']['0']         = 'Nach Geschlecht filtern';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableGenderFilter']['1']         = 'Auswählen, um diese Anrede nur für ein bestimmtes Geschlecht zu altivieren.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableRequiredFieldsFilter']['0'] = 'Nach benötigten Feldern filtern ';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableRequiredFieldsFilter']['1'] = 'Diese Anrede nur benutzen falls die ausgewählten Felder gefüllt sind.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['filter_legend']                   = 'Filtereinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['gender']['female']                = 'Weiblich';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['gender']['male']                  = 'Männlich';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['genderFilter']['0']               = 'Geschlecht';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['genderFilter']['1']               = 'Bitte wählen Sie das Geschlecht aus, für das diese Anrede benutzt wird.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['new']['0']                        = 'Neue Anrede';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['new']['1']                        = 'Neue Anrede erstellen';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['requiredFieldsFilter']['0']       = 'Benötigte Felder';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['requiredFieldsFilter']['1']       = 'Bitte wählen SIe die für diese Anrede benötigten Felder aus.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation']['0']                 = 'Anrede';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation']['1']                 = 'Bitte geben Sie die Anrede an.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation_legend']               = 'Anrede';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['show']['0']                       = 'Anrede-Details';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['show']['1']                       = 'Die Details der Anrede ID %s anzeigen.';

