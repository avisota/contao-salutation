<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:23+01:00
 */


$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['alias']['0']              = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['alias']['1']              = 'Der Alias ist eine eindeutige Referenz zur Gruppe und kann anstelle der ID benutzt werden.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['copy']['0']               = 'Gruppe duplizieren';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['copy']['1']               = 'Duplizieren Sie die Gruppe ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['delete']['0']             = 'Gruppe löschen';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['delete']['1']             = 'Löschen Sie die Gruppe ID s%.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['edit']['0']               = 'Gruppe berarbeiten';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['edit']['1']               = 'Bearbeiten Sie die Gruppe ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['editheader']['0']         = 'Gruppeneinstellungen bearbeiten';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['editheader']['1']         = 'Bearbeiten Sie die Gruppeneinstellungen ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['generate']['0']           = 'Standardgruppe erzeugen';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['generate']['1']           = 'Erzeugen Sie eine Standardgruppe, in der vordefinierte Anreden in der aktuellen Sprache enthalten sind.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['group_generated']         = 'Eine neue Standardgruppe wurde erstellt.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['new']['0']                = 'Neue Gruppe';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['new']['1']                = 'Neue Gruppe erzeugen';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['salutation_group_legend'] = 'Gruppe';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['show']['0']               = 'Gruppendetails';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['show']['1']               = 'Lassen Sie die Details der Gruppe ID %s anzeigen.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['title']['0']              = 'Titel';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['title']['1']              = 'Bitte geben Sie den Titel der Newslettergruppe an.';

