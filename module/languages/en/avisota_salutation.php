<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-core
 * @license    LGPL-3.0+
 * @filesource
 */

$GLOBALS['TL_LANG']['avisota_salutation'][0] = 'Dear Sir ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][1] = 'Dear Madam ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][2] = 'Dear Sir ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][3] = 'Dear Madam ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][4] = 'Dear Sir ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][5] = 'Dear Madam ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][6] = 'Dear Sir or Madam ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][7] = 'Dear Sir or Madam ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][8] = 'Dear Sir or Madam ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation'][9] = 'Dear subscriber';
