<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-salutation
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Module
 */
$GLOBALS['TL_LANG']['MOD']['avisota-salutation'] = array(
	'Avisota - Salutation',
	'Manage flexible and dynamic salutations for Avisota messages.'
);

/**
 * Backend modules
 */
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']        = array(
	'Salutation',
	'Manage salutations.'
);
