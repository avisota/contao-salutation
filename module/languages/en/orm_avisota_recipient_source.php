<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-subscription-recipient
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['salutation']   = array(
	'Salutation',
	'Please chose the salution used by this recipient source.'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['salutation_legend'] = 'Automated saluation';
