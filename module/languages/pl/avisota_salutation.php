<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:21+01:00
 */


$GLOBALS['TL_LANG']['avisota_salutation']['0'] = 'Szanowny Panie ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['1'] = 'Szanowna Pani ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['2'] = 'Szanowny Panie ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['3'] = 'Szanowna Pani ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['4'] = 'Szanowny Panie ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['5'] = 'Szanowna Pani ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['6'] = 'Szanowny/a Panie/Pani ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['7'] = 'Szanowny/a Panie/Pani ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['8'] = 'Szanowny/a Panie/Pani ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['9'] = 'Szanowny subskrybencie';

