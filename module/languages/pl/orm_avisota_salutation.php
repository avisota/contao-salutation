<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:23+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_salutation']['copy']['0']                       = 'Duplikuj powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['copy']['1']                       = 'Duplikuj powitanie ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['delete']['0']                     = 'Usuń powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['delete']['1']                     = 'Usuń powitanie ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['edit']['0']                       = 'Edytuj powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['edit']['1']                       = 'Edytuj powitanie ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['editheader']['0']                 = 'Edytuj ustawienia powitania';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['editheader']['1']                 = 'Edytuj ustawienia powitania ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableGenderFilter']['0']         = 'Filtru po płci';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableGenderFilter']['1']         = 'Wybierz, aby włączyć to powitanie tylko dla określonej płci.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableRequiredFieldsFilter']['0'] = 'Filtruj po wymaganych polach';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableRequiredFieldsFilter']['1'] = 'Wybierz, jeśli powitanie ma być użyte w przypadku, gdy określone pola zostały wypełnione.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['filter_legend']                   = 'Ustawienia filtrów';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['gender']['female']                = 'Kobieta';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['gender']['male']                  = 'Mężczyzna';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['genderFilter']['0']               = 'Płeć';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['genderFilter']['1']               = 'Proszę wybrać płeć, która jest wymagana dla tego powitania.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['new']['0']                        = 'Nowe powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['new']['1']                        = 'Utwórz nowe powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['requiredFieldsFilter']['0']       = 'Wymagane pola';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['requiredFieldsFilter']['1']       = 'Proszę wybrać pola, które są wymagane dla tego powitania.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation']['0']                 = 'Powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation']['1']                 = 'Proszę wprowadzić powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation_legend']               = 'Powitanie';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['show']['0']                       = 'Szczegóły powitania';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['show']['1']                       = 'Pokaż szczegóły powitania ID %s';
