<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:23+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['alias']['0']              = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['alias']['1']              = 'Alias grupy jest unikalnym odwołaniem do grupy, które może być użyte zamiast ID.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['copy']['0']               = 'Duplikuj grupę';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['copy']['1']               = 'Duplikuj grupę ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['delete']['0']             = 'Usuń grupę';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['delete']['1']             = 'Usuń grupę ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['edit']['0']               = 'Edytuj grupę';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['edit']['1']               = 'Edytuj grupę ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['editheader']['0']         = 'Edytuj ustawienia grupy';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['editheader']['1']         = 'Edytuj ustawienia grupy ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['generate']['0']           = 'Wygeneruj domyślną grupę';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['generate']['1']           = 'Wygeneruj domyślną grupę, zawierającą predefiniowane pozdrowienia w aktualnym języku.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['group_generated']         = 'Nowa domyślna grupa została wygenerowana.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['new']['0']                = 'Nowa grupa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['new']['1']                = 'Utwórz nową grupę';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['salutation_group_legend'] = 'Grupa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['show']['0']               = 'Szczegóły grupy';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['show']['1']               = 'Pokaż szczegóły grupy ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['title']['0']              = 'Tytuł';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['title']['1']              = 'Proszę wprowadzić tytuł grupy newsletterów.';
