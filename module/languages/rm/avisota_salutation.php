<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:21+01:00
 */


$GLOBALS['TL_LANG']['avisota_salutation']['0'] = 'Prezià Singur ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['1'] = 'Preziada Signura ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['2'] = 'Prezià Signur ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['3'] = 'Preziada Signura ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['4'] = 'Prezià Signur ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['5'] = 'Preziada Signura ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['6'] = 'Preziada Signura u Signur ##title## ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['7'] = 'Preziada Signur u Signur ##forename## ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['8'] = 'Preziada Signura u Signur ##surname##';
$GLOBALS['TL_LANG']['avisota_salutation']['9'] = 'Prezià abunent';

