<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T16:10:57+01:00
 */


$GLOBALS['TL_LANG']['MOD']['avisota-salutation']['0'] = 'Avisota - Salid';
$GLOBALS['TL_LANG']['MOD']['avisota-salutation']['1'] = 'Administratir a moda flexibla e dinamica ils salids per messadis dad Avisota.';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['0'] = 'Salid';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['1'] = 'Administrar ils salids.';

