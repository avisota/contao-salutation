<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-05-31T04:01:09+02:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_category']['salutation']['0']   = 'Salid';
$GLOBALS['TL_LANG']['orm_avisota_message_category']['salutation']['1']   = 'Tscherna il salid utilisà da questa funtauna da destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_message_category']['salutation_legend'] = 'Salid automatisà';
