<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:23+01:00
 */


$GLOBALS['TL_LANG']['orm_avisota_salutation']['copy']['0']                       = 'Duplitgar il salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['copy']['1']                       = 'Duplitgar il salid cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['delete']['0']                     = 'Stizzar il salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['delete']['1']                     = 'Stizzar il salid cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['edit']['0']                       = 'Modifitgar il salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['edit']['1']                       = 'Modifitgar il salid cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['editheader']['0']                 = 'Modifitgar la configuraziun dal salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['editheader']['1']                 = 'Modifitgar la configuraziun dal salid cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableGenderFilter']['0']         = 'Filtrar tenor schlattaina';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableGenderFilter']['1']         = 'Tscherna per activar quest salid be per ina schlattaina specifica.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableRequiredFieldsFilter']['0'] = 'Filtrar tenor ils champs necessaris';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['enableRequiredFieldsFilter']['1'] = 'Tscherna quai per activar quest salids be sche ils chamsp selecziunads èn emplenids';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['filter_legend']                   = 'Configuraziun da filtrar';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['gender']['female']                = 'Feminin';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['gender']['male']                  = 'Masculin';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['genderFilter']['0']               = 'Schlattaina';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['genderFilter']['1']               = 'Tscherna la schlattaina ch\'è necessaria per quest salid.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['new']['0']                        = 'Nov salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['new']['1']                        = 'Crear in nov salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['requiredFieldsFilter']['0']       = 'Champs necessaris';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['requiredFieldsFilter']['1']       = 'Tscherna ils chamsp ch\'èn necessaris per quest salid.';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation']['0']                 = 'Salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation']['1']                 = 'Endatscha il salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['salutation_legend']               = 'Salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['show']['0']                       = 'Detagls dal salid';
$GLOBALS['TL_LANG']['orm_avisota_salutation']['show']['1']                       = 'Mussar ils detagls dal salid cun l\'ID %s';

