<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:26:24+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['alias']['0']              = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['alias']['1']              = 'L\'alias da la gruppa è ina referenzà unica a la gruppa che po vegnir utilisada empè da l\'ID.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['copy']['0']               = 'Duplitgar la gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['copy']['1']               = 'Duplitgar la gruppa cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['delete']['0']             = 'Stizzar la gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['delete']['1']             = 'Stizzar la gruppa cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['edit']['0']               = 'Modifitgar la gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['edit']['1']               = 'Modifitgar la gruppa cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['editheader']['0']         = 'Modifitgar la configuraziun da la gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['editheader']['1']         = 'Modifitgar la configuraziun da la gruppa cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['generate']['0']           = 'Generar ina gruppa da standard';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['generate']['1']           = 'Generar ina gruppa da standard che cuntegna ils salids predefinids per la lingua actuala.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['group_generated']         = 'Ina nova gruppa da standard è vegnida generada.';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['new']['0']                = 'Nova gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['new']['1']                = 'Crear ina nova gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['salutation_group_legend'] = 'Gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['show']['0']               = 'Detagls da la gruppa';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['show']['1']               = 'Mussar ils detagls da la gruppa %s';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['title']['0']              = 'Titel';
$GLOBALS['TL_LANG']['orm_avisota_salutation_group']['title']['1']              = 'Endatescha il titel da la gruppa da newsletters.';
